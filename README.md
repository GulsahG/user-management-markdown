# User Management System

This document provides the requirements of the user interface of the user management system that is expected to be built.

## Requirements 
- There should be two different pages for listing the existing users, and adding a new user. 

### Homepage
- The homepage should be the first thing the user sees when they visit the application.

- It needs to have a navigation bar at the top with two buttons.
    - The first button should have the role of adding a **New User**.
    - The second button should have the role of **Hide Disabled User** from the table.
<br/>

- It needs to have a table to list all the users with the following attributes:
    1. ID
    2. User Name
    3. Email
    4. Enabled
<br/>

- The attributes of these elements should show the user that the table is **sortable** by them.

### New User Page
- The New User page should have a form with the following input names:
    1. Username
    2. Display Name
    3. Phone
    4. Email
<br/>

- It needs to have a select element with the tag **User Roles** and a suitable placeholder.
The selection should have the options:
    1. Guest
    2. Admin
    3. SuperAdmin
<br/>
    
- It needs to have a checkbox element with the tag **Enabled** initially *disabled*.
<br/>
